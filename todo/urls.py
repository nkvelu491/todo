"""todo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from app.views import DemoViewset, DemoNew
schema_view = get_swagger_view(title='app API')


router = routers.DefaultRouter()
router.register('app/serializer', DemoViewset)
router.register('app/serializersss', DemoNew)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url('app/', include('app.urls')),
    url('', include('polls.urls')),
    url(r'^swagger-docs/$',schema_view),

    url(r'^docs$', schema_view)
]
