from rest_framework import serializers
from .models import Todolist,Todo


class TodolistSerializer(serializers.ModelSerializer):

    # def get(self, instance, validated_data):
    #     try:
    #         if validated_data.get('is_disable', None) is not None:
    #             # updated_data = super().update(instance, validated_data)
    #         # else:
    #         #     data = {}
    #         #     if validated_data.get("content", None) is not None:
    #         #         data['content'] = validated_data.get('content', None)
    #             updated_data = super().update(instance, data)
    #             return updated_data
    #     except Exception as e:
    #         raise serializers.ValidationError(str(e))

    class Meta:

        model = Todolist
        # fields =('id','content')
        fields ='__all__'


class TodoSerializer(serializers.ModelSerializer):
    class Meta:

        model = Todo
        # fields =('id','content')
        fields ='__all__'
