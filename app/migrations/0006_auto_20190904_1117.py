# Generated by Django 2.2.3 on 2019-09-04 11:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20190904_1113'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Hello',
            new_name='Todo',
        ),
    ]
