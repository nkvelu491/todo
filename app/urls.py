from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
# router.register('app/serializer',DemoViewset)
# router.register(r'cutarea', UserViewSet, base_name='cutareadel')


urlpatterns = [
    # path('hello/', views.Hello, name="hi"),
    re_path(r'^helloe?/$', views.Hello, name="hi"),
    re_path(r'^data?/$', views.Data, name="hi"),

    url(r'^api/demo', views.Demo.as_view(), name='Demo'),
    # url(r'^api/Todo', views.Todo.as_view()),
    # url(r'^api/Tododestroy', views.Tododestroy.as_view()),
    # url(r'^api/demo/update/<int:pk>', views.Demo.as_view(), name='delete_event'),
    # url(r'^api/demo/get/<int:pk>', views.Demo.as_view(), name='get_event'),
    url(r'^api/test', views.test.as_view()),
]