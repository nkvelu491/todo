from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from django.views.generic import View
from rest_framework import viewsets, filters, status, generics
from rest_framework.response import Response
from .models import Todolist,Todo
from .serializers import TodolistSerializer,TodoSerializer


def Hello(request):
    my_project = "i m here"
    return render(request,"home.html", {"title":my_project} )

def Data(request):
    project = "i m not here"
    return render(request,"index.html", {"title":project} )




class Demo(APIView):

    def get(self, request):
        try:
            print(self.request.query_params)
            remove = self.request.query_params.get('remove_id')
            if remove is not  None:
                Todolist.objects.get(pk=remove).delete()
            return Response(data=Todolist.objects.all().values(), status=status.HTTP_200_OK)

        except Exception as e:
            return Response(data=e, status=status.HTTP_404_NOT_FOUND)


class test(View):
    def post (self ,request , *args ,**kwargs):
        return self.create(request, *args, **kwargs)


# class Todo(generics.ListCreateAPIView):
#     serializer_class = TodolistSerializer
#     def get_queryset(self):
#         queryset = Todolist.objects.all()
#         return queryset

class Tododestroy(generics.RetrieveDestroyAPIView):
    queryset = Todolist.objects.all()
    serializer_class = TodolistSerializer
    print(queryset)


class DemoViewset(viewsets.ModelViewSet):
    queryset = Todolist.objects.all()
    serializer_class = TodolistSerializer


class DemoNew(viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
